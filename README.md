A collection of mrjob scripts for text analysis:
================================================

spelling_assessment
------------------
Returns a weighted average spelling
assessment for all provided files using
a normalized edit distance against
a given English dictionary


concordances
------------
For each line, for each word in the line,
returns the word as key and the cyclic string
following. The 'concordance' is considering
each key (word) as an index for its context.


concordance_wc
--------------
For each output word of the concordance job,
determines a word count over all cyclic strings
it corresponds to. Returns the word as key
and the word count as value.


word_following
--------------
Gives the conditional probability
P(following word|word) for all 
words.

