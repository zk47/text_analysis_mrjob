# word_following.py
"""
    word_following
    ~~~~~~~~~~~~~~
    Mrjob computes P(following word|word) for 
    all words in the given text.

"""
from mrjob.job import MRJob

class WordFollowing(MRJob):
    
    """Returns P(following word|word) for all
    words in the given text."""
    
    GROUP = 'line_counter'
    COUNTER = 'line_counter'
    
    def line_count_mapper(self, _, line):
        """Yields the line number and line."""
        self.increment_counter(self.GROUP, self.COUNTER)
        yield self.parse_counters()[self.GROUP][self.COUNTER], line

    def line_pairs_mapper(self, line_no, line):
        """Yields pairs of lines so that following
        words can be tracked for the last word of a given
        line.

        (1, line) denotes the 2nd line
        (-1, line) is the first line
        """
        yield (line_no-1, line_no), (1,line)
        yield (line_no, line_no+1), (-1, line)


    def following_word_reducer(self, line_nos, line_tuples):
        """Yields pairs (word, word_following), 1 """
        line_tuples = list(line_tuples)
        if len(line_tuples) == 2 and len(line_tuples[0][1].strip()) > 0\
           and len(line_tuples[1][1].strip()) > 0:
            line_before = filter(lambda t: t[0] == -1, line_tuples)[0][1]
            line_after = filter(lambda t: t[0] == 1, line_tuples)[0][1]
            combined_line = ' '.join([line_before, line_after])
            for i in range(len(combined_line.split())-1):
                yield (combined_line.split()[i],
                       combined_line.split()[i+1]), 1

    def word_pairs_count_reducer(self, pair, counts):
        """Yields word, (following word, count)
        where count is the number of appearances
        of the pair of words."""
        n = sum(counts)
        word = pair[0]
        word_following = pair[1]
        yield word, (word_following, n)

    def conditional_probability_reducer(self, word,
                                        word_following_counts):
        """Yields (word, word_following), P(word_following|word)."""
        word_following_counts = list(word_following_counts)
        total_counts = sum([e[1] for e in word_following_counts])
        for wf, count in word_following_counts:
            cond_prob = count * 1.0 / total_counts
            yield (word, wf), cond_prob            

    def steps(self):
        return [self.mr(mapper=self.line_count_mapper),
                self.mr(mapper=self.line_pairs_mapper,
                        reducer=self.following_word_reducer),
                self.mr(reducer=self.word_pairs_count_reducer),
                self.mr(reducer=self.conditional_probability_reducer)]
                        
 
if __name__ == '__main__':
    WordFollowing.run()


