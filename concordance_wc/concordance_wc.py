# concordance_wc.py
"""
    concordance_wc
    ~~~~~~~~~~~~~~
    Builds on the concordance mapper- for each
    index word, outputs a word count for
    all other words existing in the lines
    in which the index word occurs.

"""
from mrjob.job import MRJob


class ConcordanceWordCount(MRJob):

    """Returns a word count for all other
    words in lines containing the given index
    word, for each index word."""

    def mapper(self, _, line):
        """Yields all cyclic permutations
        of the provided line."""
        words = line.split()
        n = len(words)
        for i in range(n):
            conc = ' '.join(words[i:n]+words[0:i])
            yield conc.split()[0], conc

    def reducer(self, index_word, lines):
        """"Word count for other words
        across lines beginning with the 
        given index word."""
        lines_word_count = {}
        for line in lines:
            other_words = line.split()[1:]
            for w in other_words:
                if w not in lines_word_count.keys():
                    lines_word_count[w] = 0
                lines_word_count[w] += 1
        yield index_word, lines_word_count


if __name__ == '__main__':
    ConcordanceWordCount.run()
