# concordances.py
"""
    concordances
    ~~~~~~~~~~~~
    Returns all concordances, eg. for each line of
    text returns all the cyclic permutations of words.
    The concordance is defined by considering the
    first word of each cyclic permutation as an index.

"""
from mrjob.job import MRJob


class Concordances(MRJob):
    
    """Returns all concordances, eg. cyclic permutations
    for each input line of text."""
    
    def mapper(self, _, line):
        """Yields all cyclic permtutation of the provided
        line."""
        words = line.split()
        n = len(words)
        for i in range(n):
            conc = ' '.join(words[i:n] + words[0:i])
            yield conc.split()[0], conc
            
            

if __name__ == '__main__':
    Concordances.run()
                     
