# spelling_assessment.py
"""
    spelling_assessment
    ~~~~~~~~~~~~~~~~~~~
    A Mrjob script that returns a weighted average spelling
    assessment of all provided files using a normalized
    edit distance against an English dictionary.

"""
from mrjob.job import MRJob
from pyxdameraulevenshtein import normalized_damerau_levenshtein_distance as\
    normalized_edit_distance


class SpellingAssessment(MRJob):

    """Gives a spelling assessment for all words across
    given provided files. Returns a final key, value pair:
    "SPELLING ASSESSMENT", <assessment_value>
     A value of 0 means no spelling errors; 1 represents
    the greatest possible number of mismatches. """
    
    def configure_options(self):
        """Option for selecting a dictionary file."""
        super(SpellingAssessment, self).configure_options()
        self.add_file_option('--dictionary')
        
    def mapper(self, _, line):
        """Yields each word as key with a value of 1."""
        words = line.split()
        for w in words:
            yield w, 1

    def reducer_init(self):
        """Make dictionary available as an attribute."""
        f = open(self.options.dictionary, 'r')
        self.dictionary = set()
        for line in f:
            self.dictionary.update([line.strip()])
                    
    def reducer(self, word, units):
        """Returns as value the spelling error for the given word,
        and the number of appearances of that word. Converts the word
        to lower-case."""
        count = len(list(units))
        weighted_spelling_contribution = count * \
                                         self._min_norm_edit_distance(
                                             word.lower(), self.dictionary)
        yield 1, (weighted_spelling_contribution, count)

    def get_assessment(self, key, values):
        """Yields the final spelling assessment across all files."""
        values = list(values)
        yield 'SPELLING ASSESSMENT', sum([e[0]*e[1] for e in values]) / (
            sum([e[1] for e in values]))
    
    def _min_norm_edit_distance(self, word, dictionary):
        """Given a set of words in <dictionary>,
        returns a normalized edit distance (a value between 
        0 and 1) between <word> and its closest match
        in <dictionary>."""
        try:
            return min([normalized_edit_distance(word, d) for d \
                        in dictionary])
        except:
            return 1.0        
        
    def steps(self):
        return [self.mr(mapper=self.mapper,
                        reducer_init=self.reducer_init,
                        reducer=self.reducer),
                self.mr(reducer=self.get_assessment)]

        

if __name__ == '__main__':
    SpellingAssessment.run()
